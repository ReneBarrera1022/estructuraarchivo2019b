//Alumno: René Alejandro Barrera Ramírez
#include <iostream>
#include <string>

using namespace std;


struct Escuela{
    char inicialEscuela;
    float matricula;
    float semestre;
    float calificacion;
};
struct persona{
    string name;
    string pais;
    string estadoCivil;
    string email;
    int age;
    Escuela inicialEscuela;
};

int main(){
    persona per;
    struct Escuela esc;
    esc.matricula = 123.10;
    esc.semestre = 4.00;
    esc.calificacion = 10.12;

    per.inicialEscuela = esc;

    cout << "Nombre: "; cin >> per.name;
    cout << "Edad: "; cin >> per.age;
    cout << "Paìs: ";  cin >> per.pais;
    cout << "Correo: "; cin >> per.email;
    cout << "Estado civil: "; cin >> per.estadoCivil;
    cout << "Hola " << per.name << " tienes " << per.age << " años, eres de " << per.pais << ", tu correo electrónico es " << per.email << " y tu estado civil es " << per.estadoCivil << endl;
    cout << "Tu calificacion es "<<per.inicialEscuela.calificacion << " tú matrícula es " <<per.inicialEscuela.matricula;

    return 0;
}



