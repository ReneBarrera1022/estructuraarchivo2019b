//Alumno: René Alejandro Barrera Ramírez
#include <iostream>
#include <string>

using namespace std;

struct persona{
    string name;
    string pais;
    string estadoCivil;
    string email;
    int age;
};

int main(){
    persona per;
    cout << "Nombre: "; cin >> per.name;
    cout << "Edad: "; cin >> per.age;
    cout << "Paìs: ";  cin >> per.pais;
    cout << "Correo: "; cin >> per.email;
    cout << "Estado civil: "; cin >> per.estadoCivil;
    cout << "Hola " << per.name << " tienes " << per.age << " años, eres de " << per.pais << ", tu correo electrónico es " << per.email << " y tu estado civil es " << per.estadoCivil << endl;
    return 0;
}


